---
#######################################################################
#                                                                     #
#   Parse Jinja template                                              #
#                                                                     #
#######################################################################
- name: "stack | ecs | nlb | debug | params"
  tags: [ "aws", "ecs:nlb", "aws:ecs:nlb", "ecs:nlb:stack", "aws:ecs:nlb:stack" ]
  debug: var=aws_ecs_nlb_params
  when: aws_ecs_nlb_params.debug

- name: "stack | ecs | nlb | template | tempfile | {{ environment_abbr }}-{{ nlb.role }}"
  tags: [ "aws", "ecs:nlb", "aws:ecs:nlb", "ecs:nlb:stack", "aws:ecs:nlb:stack" ]
  tempfile:
    state: file
    suffix: yaml
  register: cf_template

- name: "stack | ecs | nlb | template | expand"
  tags: [ "aws", "ecs:nlb", "aws:ecs:nlb", "ecs:nlb:stack", "aws:ecs:nlb:stack" ]
  template:
    src: "{{ role_path }}/templates/cf-ecs-nlb.yaml.j2"
    dest: "{{ cf_template.path }}"
  vars:
    listeners: "{{ nlb.listeners }}"

#######################################################################
#                                                                     #
#   ECS NLB                                                           #
#                                                                     #
#######################################################################
- name: "stack | ecs | nlb | set_fact | cftags_ecs_nlb"
  tags: [ "aws", "ecs:nlb", "aws:ecs:nlb", "ecs:nlb:stack", "aws:ecs:nlb:stack" ]
  set_fact:
    cftags_ecs_nlb: "{{ cftags_ecs_nlb | default({}) | combine({tag_prefix + ':' + item.key: item.value}) | combine(cloudformation_tags | default({})) }}"
  with_dict:
    owner           : "{{ owner }}"
    environment     : "{{ environment_type }}"
    environment_abbr: "{{ environment_abbr }}"
    account         : "{{ account_name }}"
    role            : "ecs-nlb"

- name: "stack | ecs | nlb | cloudformation | {{ environment_abbr }}-{{ nlb.role }}"
  tags: [ "aws", "ecs:nlb", "aws:ecs:nlb", "ecs:nlb:stack", "aws:ecs:nlb:stack" ]
  cloudformation:
    state              : present
    template           : "{{ cf_template.path }}"
    stack_name         : "{{ environment_abbr }}-ecs-nlb-{{ nlb.role }}"
    create_changeset   : "{{ create_changeset | default(True) }}"
    region             : "{{ aws_region }}"
    tags               : "{{ cftags_ecs_nlb }}"
    template_parameters:
      VPCID          : "{{ vpc_stack.stack_outputs.VpcId }}"
      Subnets        : "{{ vpc_stack.stack_outputs.SubnetsPrivate if (nlb.nlb_scheme | default(nlb_scheme)) == 'internal' else vpc_stack.stack_outputs.SubnetsPublic }}"
      NLBScheme      : "{{ nlb.nlb_scheme | default(nlb_scheme) }}"
      ECSClusterName : "{{ nlb.ecs_cluster_name }}"
      EnvironmentAbbr: "{{ environment_abbr }}"

- name: "stack | ecs | nlb | debug | register"
  tags: [ "aws", "ecs:nlb", "aws:ecs:nlb", "ecs:nlb:stack", "aws:ecs:nlb:stack" ]
  cloudformation_facts:
    stack_name: "{{ environment_abbr }}-ecs-nlb-{{ nlb.role }}"
  register: result
  when: aws_ecs_nlb_params.debug

- name: "stack | ecs | nlb | debug | print"
  tags: [ "aws", "ecs:nlb", "aws:ecs:nlb", "ecs:nlb:stack", "aws:ecs:nlb:stack" ]
  debug: var=result.ansible_facts.cloudformation[environment_abbr+'-ecs-nlb-'+nlb.role]
  when: aws_ecs_nlb_params.debug
