AWS ECS NLB
=========
This role is part of the [Mirabeau Cloud Framework](https://gitlab.com/mirabeau/cloud-framework/)

Create NLBs with Cloudformation. AWS resources that will be created are:
 * ElasticLoadbalancer V2
 * Securitygroup for the NLB
 * SecurityGroup Ingress rule allowing NLB to connect to provided ECS cluster SG

Requirements
------------
Ansible version 2.5.4 or higher  
Python 2.7.x

Required python modules:
* boto
* boto3
* awscli

Dependencies
------------
 * aws-vpc
 * aws-securitygroups
 * aws-ecs-cluster

Role Variables
--------------
### _General_
The following params should be available for Ansible during the rollout of this role:
```yaml
aws_region      : <aws region, eg: eu-west-1>
owner           : <owner, eg: mirabeau>
account_name    : <aws account name>
account_abbr    : <aws account generic environment>
environment_type: <environment>
environment_abbr: <environment abbriviation>
```
Role Defaults
--------------
```yaml
cloudformation_tags : {}
tag_prefix          : "mcf"
aws_ecs_nlb_params:
  debug             : False
  nlbs:
    - role            : "ecs-nlb"
      nlb_scheme      : "internal" # Default NLB scheme is 'internal' (alternative is internet-facing)
      ecs_cluster_name: "{{ aws_ecs_cluster_params.cluster_name }}"
      listeners       : {}
```

Example Playbook
----------------
```yaml
---
- hosts: localhost
  connection: local
  gather_facts: False
  vars:
    aws_region      : "eu-west-1"
    owner           : "myself"
    account_name    : "my-dta"
    account_abbr    : "dta"
    environment_type: "test"
    environment_abbr: "tst"
    internal_route53:
      domainname: my.cloud
    aws_vpc_params:
      network               : "10.10.10.0/24"
      public_subnet_weight  : 1
      private_subnet_weight : 3
      database_subnet_weight: 1
    aws_ecs_cluster_params:
      keypair: "mykeypair"
    aws_ecs_nlb:
      nlbs:
        - role            : "ecs-nlb"
          nlb_scheme      : "internal"
          ecs_cluster_name: "{{ aws_ecs_cluster_params.cluster_name }}"
          listeners:
            "80": {}

  pre_tasks:
    - name: Get latest AWS AMI's
      include_role:
        name: aws-utils
        tasks_from: get_aws_amis

  roles:
    - aws-setup
    - aws-iam
    - aws-vpc
    - env-acl
    - aws-securitygroups
    - aws-lambda
    - aws-ecs-cluster
    - aws-ecs-nlb
```
License
-------
GPLv3

Author Information
------------------
Lotte-Sara Laan <llaan@mirabeau.nl>  
Wouter de Geus <wdegeus@mirabeau.nl>  
Rob Reus <rreus@mirabeau.nl>
